#!/bin/bash

./scripts/feeds update -a
./scripts/feeds install -a

#patching wifidog
if [ ! -d feeds/packages/net/wifidog/patches ]; then
	mkdir -p feeds/packages/net/wifidog/patches
fi
cp repu1sion/001-wifidog.patch feeds/packages/net/wifidog/patches
if [ $? -eq 0 ]; then
	echo "wifidog patch installed successfully"
else
	echo "failed to install watchdog patch!!!"
fi
sleep 3

cp openwrt_config .config
make menuconfig

